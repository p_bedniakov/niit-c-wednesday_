/*Sorting*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
	char line[80], x;
	int l=0, r=0, n=0;
	printf("Enter a line:\n");
	scanf("%s", &line);
	r=strlen(line)-1;
	while(l<r)
		{
			if(line[l]>='0' && line[l]<='9')
			{
				for(;r>l;r--)
				{
					if((line[r]>='a' && line[r]<='z') || (line[r]>='A' && line[r]<='Z'))
					{
						x=line[l];
						line[l]=line[r];
						line[r]=x;
						break;
					}
				}
			}
			l++;
		}
		for(n=0;line[n]!=0;n++)
			putchar(line[n]);
		printf("\n");
	return 0;
}